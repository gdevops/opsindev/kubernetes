
.. index::
   pair: Kubernetes ; Service
   ! Service

.. _kubernetes_service:

=====================
Kubernetes service
=====================

.. seealso::

   - https://kubernetes.io/docs/concepts/services-networking/service/




Motivation
============

Kubernetes Pods are mortal.

They are born and when they die, they are not resurrected.

If you use a Deployment to run your app, it can create and destroy Pods
dynamically.

Each Pod gets its own IP address, however in a Deployment, the set of
Pods running in one moment in time could be different from the set of
Pods running that application a moment later.

This leads to a problem: if some set of Pods (call them “backends”)
provides functionality to other Pods (call them “frontends”) inside
your cluster, how do the frontends find out and keep track of which IP
address to connect to, so that the frontend can use the backend part
of the workload?

Enter Services
