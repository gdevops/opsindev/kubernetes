
.. index::
   pair: Kubernetes ; Concepts

.. _kubernetes_concepts:

=====================
Concepts
=====================

.. seealso::

   - https://kubernetes.io/docs/concepts/


.. toctree::
   :maxdepth: 3

   overview/overview
   service/service
   storage/storage
