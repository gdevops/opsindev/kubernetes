


.. index::
   pair: Kubernetes ; Storage


.. _kubernetes_storage:

=====================
Kubernetes storage
=====================


.. seealso::

   - https://kubernetes.io/docs/concepts/storage/
