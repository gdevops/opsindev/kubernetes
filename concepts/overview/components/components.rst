.. index::
   pair: Kubernetes ; Components
   pair: Kubernetes ; Cluster
   pair: Kubernetes ; Worker machine
   pair: Kubernetes ; Node
   pair: Kubernetes ; Control plane
   pair: Kubernetes ; high-availability
   pair: Kubernetes ; fault-tolerance
   pair: Control plane ; kube-apiserver
   pair: Control plane ; etcd
   pair: Control plane ; kube-scheduler
   pair: Control plane ; kube-controller-manager
   pair: Control plane ; cloud-controller-manager
   pair: Node component ; kubelet
   pair: Node component ; kube-proxy
   pair: Node component ; Container Runtime

.. _kubernetes_concepts_components:

================================
Kubernetes components
================================

.. seealso::

   - https://kubernetes.io/docs/concepts/overview/components/



**Kubernetes components**
===========================

When you deploy Kubernetes, **you get a cluster**.

A Kubernetes cluster consists of a set of **worker machines**, called **nodes**,
that run containerized applications.

Every cluster has at least one worker node.

The worker node(s) host the Pods that are the components of the
application workload.

The **control plane** manages the worker nodes and the Pods in the cluster.

In production environments, the **control plane** usually runs across
multiple computers and a cluster usually runs multiple nodes, providing
**fault-tolerance** and **high availability**.

This document outlines the various components you need to have a
complete and working Kubernetes cluster.


.. figure:: components-of-kubernetes.png
   :align: center

   https://kubernetes.io/docs/concepts/overview/components/


.. _control_plane_components:

**Control Plane Components**
===============================

.. seealso::

   - https://kubernetes.io/docs/admin/high-availability/

The control plane’s components make global decisions about the cluster
(for example, scheduling), as well as detecting and responding to
cluster events (for example, starting up a new pod when a deployment’s
replicas field is unsatisfied).

**Control plane components** can be run on any machine in the cluster.

However, for simplicity, set up scripts typically start all control plane
components on the same machine, and do not run user containers on this
machine.

See `Building High-Availability Clusters`_ for an example multi-master-VM setup.
kube-apiserver

.. _`Building High-Availability Clusters`:  https://kubernetes.io/docs/admin/high-availability/


.. _kube_apiserver:

kube-apiserver
---------------

The API server is a component of the Kubernetes control plane that
exposes the Kubernetes API.

The API server is the front end for the Kubernetes control plane.

The main implementation of a Kubernetes API server is **kube-apiserver**.

**kube-apiserver** is designed to scale horizontally—that is, it scales
by deploying more instances.
You can run several instances of kube-apiserver and balance traffic
between those instances.


.. kube_etcd:

etcd
------

.. seealso::

   - https://github.com/etcd-io/etcd
   - https://github.com/etcd-io/etcd/tree/master/etcdctl
   - https://etcd.io/docs/
   - https://kubernetes.io/docs/tasks/administer-cluster/configure-upgrade-etcd/#backing-up-an-etcd-cluster

Consistent and highly-available key value store used as Kubernetes’
backing store for all cluster data.

If your Kubernetes cluster uses etcd as its backing store, make sure
you have a `back up plan`_ for those data.

You can find in-depth information about etcd in the official documentation_.


.. _`back up plan`: https://kubernetes.io/docs/tasks/administer-cluster/configure-upgrade-etcd/#backing-up-an-etcd-cluster
.. _documentation: https://etcd.io/docs/

.. _kube_scheduler:

kube-scheduler
---------------

**Control plane** component that watches for newly created Pods with no
assigned node , and selects a node for them to run on.

Factors taken into account for scheduling decisions include: individual
and collective resource requirements, hardware/software/policy constraints,
affinity and anti-affinity specifications, data locality, inter-workload
interference, and deadlines


.. _kube_controller_manager:

kube-controller-manager
----------------------------

**Control Plane** component that runs controller processes.

Logically, each controller is a separate process, but to reduce
complexity, they are all compiled into a single binary and run
in a single process.

These controllers include:

- **Node controller**
  Responsible for noticing and responding when nodes go down.
- **Replication controller**
  Responsible for maintaining the correct number of pods for every
  replication controller object in the system.
- **Endpoints controller**
  Populates the Endpoints object (that is, joins Services & Pods).
- **Service Account & Token controllers**
  Create default accounts and API access tokens for new namespaces


.. _cloud_controller_manager:

cloud-controller-manager
----------------------------

A Kubernetes **control plane** component that embeds cloud-specific
control logic.

The **cloud controller manager** lets you link your cluster into your
cloud provider’s API, and separates out the components that interact
with that cloud platform from components that just interact with your
cluster.

The **cloud-controller-manager** only runs controllers that are specific
to your cloud provider.
If you are running Kubernetes on your own premises, or in a learning
environment inside your own PC, the cluster does not have a cloud
controller manager.

As with the :ref:`kube-controller-manager <kube_controller_manager>`, the **cloud-controller-manager**
combines several logically independent control loops into a single
binary that you run as a single process.
You can scale horizontally (run more than one copy) to improve
performance or to help tolerate failures.

The following controllers can have cloud provider dependencies:

- **Node controller**
  For checking the cloud provider to determine if a node has been deleted
  in the cloud after it stops responding
- **Route controller**
  For setting up routes in the underlying cloud infrastructure
- **Service controller**
  For creating, updating and deleting cloud provider load balancers


.. _node_components:

**Node Components**
===============================


.. figure:: components-of-kubernetes.png
   :align: center

   https://kubernetes.io/docs/concepts/overview/components/


**Node components** run on every node, maintaining running pods and
providing the Kubernetes runtime environment.


.. _kubelet:

kubelet
----------

An agent that runs on each node in the **cluster**.

**It makes sure that containers are running in a Pod.**

The kubelet takes a set of PodSpecs that are provided through various
mechanisms and ensures that the containers described in those PodSpecs
are running and healthy.

The kubelet doesn’t manage containers which were not created by Kubernetes


.. _kube_proxy:

kube-proxy
-----------

**kube-proxy** is a network proxy that runs on each node in your cluster,
implementing part of the **Kubernetes Service concept**.

**kube-proxy** maintains network rules on nodes.
These network rules allow network communication to your Pods from
network sessions inside or outside of your **cluster**.

**kube-proxy** uses the operating system packet filtering layer if
there is one and it’s available.
Otherwise, kube-proxy forwards the traffic itself


.. _container_runtime:

Container Runtime
--------------------

.. seealso::

   - https://github.com/kubernetes/community/blob/master/contributors/devel/sig-node/container-runtime-interface.md

The **container runtime** is the software that is responsible for
running containers.

Kubernetes supports several container runtimes: Docker , containerd ,
CRI-O , and any implementation of the `Kubernetes CRI (Container Runtime Interface)`_.

.. _`Kubernetes CRI (Container Runtime Interface)`: https://github.com/kubernetes/community/blob/master/contributors/devel/sig-node/container-runtime-interface.md


.. _kube_addons:

**Addons**
============

.. seealso::

   - https://kubernetes.io/docs/concepts/cluster-administration/addons/

**Addons** use Kubernetes resources (DaemonSet , Deployment , etc) to
implement **cluster features**.

Because these are providing cluster-level features, namespaced resources
for addons belong within the **kube-system** namespace.

Selected addons are described below; for an extended list of available
addons, please see Addons_


.. _Addons: https://kubernetes.io/docs/concepts/cluster-administration/addons/


.. _kube_dns:

DNS
------

.. seealso::

   - https://kubernetes.io/docs/concepts/services-networking/dns-pod-service/

While the other addons are not strictly required, all Kubernetes clusters
should have cluster DNS, as many examples rely on it.

Cluster DNS is a DNS server, in addition to the other DNS server(s) in
your environment, which serves DNS records for Kubernetes services.

Containers started by Kubernetes automatically include this DNS server
in their DNS searches


.. kube__web_ui:

Web UI (Dashboard)
----------------------

.. seealso::

   - https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/

Dashboard_ is a general purpose, web-based UI for Kubernetes clusters.

It allows users to manage and troubleshoot applications running in
the cluster, as well as the cluster itself.

.. _Dashboard:  https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/


.. _container_resource_monitoring:

Container Resource Monitoring
---------------------------------

.. seealso::

   - https://kubernetes.io/docs/tasks/debug-application-cluster/resource-usage-monitoring/

`Container Resource Monitoring`_ records generic **time-series metrics** about
containers in a central database, and provides a UI for browsing that data.

.. _`Container Resource Monitoring`: https://kubernetes.io/docs/tasks/debug-application-cluster/resource-usage-monitoring/


.. cluster_level_logging:

Cluster-level Logging
-----------------------

.. seealso::

   - https://kubernetes.io/docs/concepts/cluster-administration/logging/


A `cluster-level logging`_ mechanism is responsible for saving container
logs to a central log store with search/browsing interface


.. _`cluster-level logging`:  https://kubernetes.io/docs/concepts/cluster-administration/logging/
