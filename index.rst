.. Kubernetes tutorial documentation master file, created by
   sphinx-quickstart on Thu Apr  5 22:00:08 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>
   
   
|FluxWeb| `RSS <https://gdevops.frama.io/opsindev/kubernetes/rss.xml>`_

.. _kubernetes_tuto:

=================================================================================
Opsindev Kubernetes (Production-Grade Container Scheduling and Management)
=================================================================================

- https://gitlab.com/everyonecancontribute/kubernetes


.. figure:: _static/Kubernetes.png
   :align: center

   Logo Kubernetes


.. figure:: _static/CNCF_TrailMap_latest.png
   :align: center

   https://raw.githubusercontent.com/cncf/trailmap/master/CNCF_TrailMap_latest.png



.. figure:: _static/cncf_orchestration.png
   :align: center

   https://landscape.cncf.io/category=scheduling-orchestration&format=card-mode&grouping=category


.. figure:: _static/cncf_kubernetes_2021_08.png
   :align: center

   https://landscape.cncf.io/category=scheduling-orchestration&format=card-mode&grouping=category&selected=kubernetes


- https://framagit.org/gdevops/opsindev/tuto_kubernetes
- https://en.wikipedia.org/wiki/Kubernetes
- https://kubernetes.io/
- https://github.com/kubernetes/kubernetes
- https://x.com/kubernetesio
- https://kubernetes.io/feed.xml
- https://discuss.kubernetes.io/
- https://landscape.cncf.io/
- https://landscape.cncf.io/format=landscape
- https://landscape.cncf.io/landscape=scheduling-orchestration
- https://qconuk2019.container.training
- https://landscape.cncf.io/category=scheduling-orchestration&format=card-mode&grouping=category&selected=kubernetes



.. toctree::
   :maxdepth: 6

   concepts/concepts
   docs/docs
   news/news
   versions/versions
