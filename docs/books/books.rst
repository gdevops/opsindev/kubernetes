


.. index::
   pair: Kubernetes ; Books


.. _kubernetes_books:

=====================
Kubernetes books
=====================




Kubernetes Cookbook: Building Cloud-Native Applications
========================================================


.. seealso::

   - https://x.com/sebgoa
   - https://www.linkedin.com/pulse/how-service-providers-can-use-kubernetes-scale-nfv-jason-hunt/
   - http://shop.oreilly.com/product/0636920064947.do
