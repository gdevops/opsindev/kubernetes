


.. index::
   pair: Kubernetes ; Why


.. _kubernetes_why:

=====================
Why Kubernetes ?
=====================

.. seealso::

   - https://landscape.cncf.io/format=landscape
   - https://landscape.cncf.io/landscape=scheduling-orchestration




How service providers can use Kubernetes to scale NFV transformation
======================================================================


.. seealso::

   - https://www.linkedin.com/pulse/how-service-providers-can-use-kubernetes-scale-nfv-jason-hunt/
   - https://x.com/kubernetesio/status/981913236122431489


::

    In ONAP’s experience, running on top of Kubernetes, rather than
    virtual machines, can reduce installation time from hours or
    weeks to just 20 minutes.
