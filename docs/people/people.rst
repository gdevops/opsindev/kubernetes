
.. index::
   pair: Kubernetes ; people

.. _kubernetes_people:

=====================================
Kubernetes people
=====================================

.. toctree::
   :maxdepth: 3


   jpetazzo/jpetazzo
   luc_juggery/luc_juggery
