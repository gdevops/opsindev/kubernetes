
.. index::
   pair: jpetazzo ; people
   ! Jérôme Petazzoni

.. _kub_jpetazzo:

==================================================
Jérôme Petazzoni (https://x.com/jpetazzo)
==================================================

.. seealso::

   - https://x.com/jpetazzo
   - https://github.com/jpetazzo
   - https://github.com/jpetazzo/container.training
   - https://github.com/enix
   - https://github.com/enix/container.training
