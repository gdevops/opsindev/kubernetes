
.. index::
   pair: Kubernetes ; Gituma

.. _kubernetes_gituma:

=====================================
Kubernetes tutorial by Mark Gituma
=====================================

.. seealso::

   - https://x.com/MarkGituma
   - https://medium.com/@markgituma/kubernetes-local-to-production-with-django-1-introduction-d73adc9ce4b4
   - https://medium.com/@markgituma/kubernetes-local-to-production-with-django-2-docker-and-minikube-ba843d858817
   - https://medium.com/@markgituma/kubernetes-local-to-production-with-django-3-postgres-with-migrations-on-minikube-31f2baa8926e
   - https://medium.com/@markgituma/kubernetes-local-to-production-with-django-4-celery-with-redis-and-flower-df48ab9896b7
   - https://medium.com/@markgituma/kubernetes-local-to-production-with-django-5-deploy-to-aws-using-kops-with-rds-postgres-6f913bcab622
   - https://medium.com/@markgituma/kubernetes-local-to-production-with-django-6-add-prometheus-grafana-monitoring-with-helm-926fafbe1d
