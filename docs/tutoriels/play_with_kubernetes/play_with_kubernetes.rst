
.. index::
   pair: Play with Kubernetes ; Tutorial

.. _play_with_kubernetes:

=======================
Play with Kubernetes
=======================


.. seealso::

    - https://training.play-with-kubernetes.com/
    - https://paris.container.training/kube.html
    - https://avril2018.container.training/kube.yml.html
