
.. index::
   pair: charts ; argo cd
   ! argo cd

.. _argo_cd:

===========================================================================
Argo CD is a declarative, GitOps continuous delivery tool for Kubernetes
===========================================================================

.. seealso::

   - https://github.com/argoproj
   - https://github.com/argoproj/argo-cd
   - https://x.com/argoproj
   - https://blog.argoproj.io/feed
   - https://argoproj.github.io/argo-cd/
   - https://argoproj.github.io/argo-cd/getting_started/
   - https://argoproj.github.io/argo-cd/user_guide/
   - https://argoproj.github.io/argo-cd/developer-guide/
   - https://landscape.cncf.io/category=continuous-integration-delivery&format=card-mode&grouping=category&selected=argo

.. figure:: argo_cd_logo.png
   :align: center


.. figure:: cncf_argo.png
   :align: center

   https://landscape.cncf.io/category=continuous-integration-delivery&format=card-mode&grouping=category&selected=argo




Getting Started
================

Quick Start::

    kubectl create namespace argocd
    kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

Follow our `getting started guide`_.

Further `user oriented documentation`_ is provided for additional features.

`Developer oriented documentation`_ is available for people interested in
building third-party integrations.


.. _`getting started guide`: https://argoproj.github.io/argo-cd/getting_started/
.. _`user oriented documentation`: https://argoproj.github.io/argo-cd/user_guide/
.. _`Developer oriented documentation`: https://argoproj.github.io/argo-cd/developer-guide/

Features
============

- Automated deployment of applications to specified target environments
- Support for multiple config management/templating tools (Kustomize, Helm, Ksonnet, Jsonnet, plain-YAML)
- Ability to manage and deploy to multiple clusters
- SSO Integration (OIDC, OAuth2, LDAP, SAML 2.0, GitHub, GitLab, Microsoft, LinkedIn)
- Multi-tenancy and RBAC policies for authorization
- Rollback/Roll-anywhere to any application configuration committed in Git repository
- Health status analysis of application resources
- Automated configuration drift detection and visualization
- Automated or manual syncing of applications to its desired state
- Web UI which provides real-time view of application activity
- CLI for automation and CI integration
- Webhook integration (GitHub, BitBucket, GitLab)
- Access tokens for automation
- PreSync, Sync, PostSync hooks to support complex application rollouts (e.g.blue/green & canary upgrades)
- Audit trails for application events and API calls
- Prometheus metrics
- Parameter overrides for overriding ksonnet/helm parameters in Git


Versions
============

.. toctree::
   :maxdepth: 3

   versions/versions
