

.. _argo_cd_versions:

===========================================================================
Argo CD versions
===========================================================================

.. seealso::

   - https://github.com/argoproj/argo-cd/releases
   - https://github.com/argoproj/argo-cd/blob/master/CHANGELOG.md

.. toctree::
   :maxdepth: 3

   1.5.2/1.5.2
