.. index::
   pair: Kubernetes ; Awsome

.. _awsome_kubernetes:

=====================
awsome Kubernetes
=====================

.. seealso::

   - https://ramitsurana.github.io/awesome-kubernetes/
   - https://github.com/operator-framework/awesome-operators
