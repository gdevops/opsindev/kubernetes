
.. index::
   pair: Commands ; kubectl

.. _kubectl_commands:

==============================================================================================
kubectl commands
==============================================================================================

.. seealso::

   - https://kubernetes.io/docs/reference/kubectl/overview/

.. toctree::
   :maxdepth: 3

   api_resources/api_resources
   apply/apply
   cluster_info/cluster_info
   completion/completion
   config/config
   create/create
   delete/delete
   describe/describe
   exec/exec
   explain/explain
   get/get
   help/help
   logs/logs
   port_forward/port_forward
   proxy/proxy
   run/run
