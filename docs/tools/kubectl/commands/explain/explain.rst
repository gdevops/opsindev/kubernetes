
.. index::
   pair: explain ; kubectl
   pair: pod ; shell interactif
   ! kubectl explain

.. _kubectl_explain:

==============================================================================================
kubectl **explain** (Resources documentation)
==============================================================================================





kubectl **explain**
====================================

::

    kubectl explain pod


::

    kubectl explain pod.spec

::

    kubectl explain pod.spec.tolerations

::

    kubectl explain pod.spec.containers.command
