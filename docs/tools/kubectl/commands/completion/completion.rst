
.. index::
   pair: completion ; kubectl
   ! kubectl completion

.. _kubectl_completion:

==============================================================================================
kubectl **completion**
==============================================================================================





kubectl **completion**
====================================


sudo apt-get install bash-completion
======================================

::

    sudo apt-get install bash-completion

::

    source < (kubectl completion bash)
