
.. index::
   pair: exec ; kubectl
   pair: pod ; shell interactif
   ! kubectl exec

.. _kubectl_exec:

==============================================================================================
kubectl **exec**
==============================================================================================





kubectl **exec**
====================================

::

    kubectl exec wwww -- nginx -v


Shell interactif dans un pod
=============================

::

    kubectl exec -t -i www -- /bin/bash
