
.. index::
   pair: run ; kubectl
   pair: run ;dry-run
   ! dry-run
   ! kubectl run

.. _kubectl_run:

==============================================================================================
kubectl **run** + **dry-run**
==============================================================================================





kubectl **run**
====================================

::

    kubectl run www --image=nginx:1.16-alpine --restart=newer


::

    kubectl run www --image=nginx:1.16-alpine --restart=newer --dry-run -o yaml
