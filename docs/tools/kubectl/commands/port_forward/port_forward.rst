
.. index::
   pair: port-forward ; kubectl
   ! kubectl port-forward

.. _kubectl_port_forward:

==============================================================================================
kubectl **port-forward**
==============================================================================================





kubectl **port-forward**
====================================

::

    kubectl port-forward <pod_name> <host_port>:<container_port>



::

    kubectl port-forward www 8080:80
