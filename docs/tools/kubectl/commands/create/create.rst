
.. index::
   pair: create ; kubectl
   ! kubectl create

.. _kubectl_create:

==============================================================================================
kubectl **create**
==============================================================================================





kubectl **create**
====================================


::

    kubectl create -f pod_specification.yaml


::

    kubectl create -f nginx-pod.yaml
