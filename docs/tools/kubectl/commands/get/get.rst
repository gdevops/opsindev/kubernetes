
.. index::
   pair: get ; kubectl
   pair: get ; jsonpath
   pair: get ; custom-columns
   ! kubectl get

.. _kubectl_get:

==============================================================================================
kubectl **get** + jsonpath + custom-columns
==============================================================================================

.. seealso::

   - https://kubernetes.io/fr/docs/reference/kubectl/jsonpath




kubectl **get**
====================================


::

    kubectl get pods -n kube-system


::

    kubectl get pods

::

    kubectl get po/www -o yaml

::

    kubectl get po/www -o json


::

    kubectl get po/www -o jsonpath='{.spec.containers[0].image}'


::

    kubectl get pods -n kube-system -o jsonpath='{range.items[*]}{.status.containerStatuses[0].imageID}{"\n"}{end}'


::

    kubectl get pods -o custom-columns='NAME:metadata.name'


::

    kubectl get pods -n kube-system -o custom-columns='NAME:metadata.name,IMAGES:spec.containers[*].image'


::

    kubectl get all -n kubernetes-dashboard


.. _aliases_kubectl_get_ref:

Liste des alias avec kubectl get
==================================

.. seealso::

   - :ref:`aliases_kubectl_get_ref`
