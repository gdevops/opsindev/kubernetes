
.. index::
   pair: describe ; kubectl
   ! kubectl describe

.. _kubectl_describe:

==============================================================================================
kubectl **describe**
==============================================================================================





kubectl **describe**
====================================


::

    kubectl describe pod <pod_name>


::

    kubectl describe po/www
