
.. index::
   pair: config ; kubectl
   ! kubectl config

.. _kubectl_config:

==============================================================================================
kubectl **config**
==============================================================================================





kubectl **config**
====================================


::

    kubectl config get-contexts


::

    kubectl config current-context


::

    kubectl config use-context minikube


::

    kubectl config view
