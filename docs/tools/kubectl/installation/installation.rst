
.. index::
   pair: Installation ; kubectl

.. _kubectl_installation:

==============================================================================================
kubectl installation
==============================================================================================

.. seealso::

   - https://kubernetes.io/docs/tasks/tools/install-kubectl/




Install kubectl on Linux
===========================

::

    curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl

::

      % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                     Dload  Upload   Total   Spent    Left  Speed
    100 41.9M  100 41.9M    0     0  9376k      0  0:00:04  0:00:04 --:--:-- 9875k


Make the kubectl binary executable
=====================================

::

    chmod +x ./kubectl

Move the binary in to your PATH
=================================

::

    sudo mv ./kubectl /usr/local/bin/kubectl



Test to ensure the version you installed is up-to-date **kubectl version --client**
======================================================================================

::

    kubectl version --client

::

    Client Version: version.Info{Major:"1", Minor:"18", GitVersion:"v1.18.2", GitCommit:"52c56ce7a8272c798dbc29846288d7cd9fbae032", GitTreeState:"clean", BuildDate:"2020-04-16T11:56:40Z", GoVersion:"go1.13.9", Compiler:"gc", Platform:"linux/amd64"}


Verifying kubectl configuration
==================================

In order for kubectl to find and access a Kubernetes cluster, it needs
a kubeconfig file, which is created automatically when you create a
cluster using kube-up.sh or successfully deploy a Minikube cluster.

By default, kubectl configuration is located at ~/.kube/config.

Check that kubectl is properly configured by getting the cluster state


    kubectl cluster-info

::

    Kubernetes master is running at https://172.17.0.2:8443
    KubeDNS is running at https://172.17.0.2:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

    To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
