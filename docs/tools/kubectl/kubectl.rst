
.. index::
   ! kubectl

.. _kubectl:

==============================================================================================
**kubectl** (kubectl controls the Kubernetes cluster manager)
==============================================================================================

.. seealso::

   - :ref:`minikube_kubectl`
   - https://github.com/kubernetes/kubectl
   - https://kubernetes.io/docs/reference/kubectl/overview/


.. figure:: kubectl-logo-medium.png
   :align: center

   https://github.com/kubernetes/kubectl


.. toctree::
   :maxdepth: 3

   installation/installation
   commands/commands
   versions/versions
