
.. index::
   pair: kubectl; Versions

.. _kubectl_versions:

==============================================================================================
kubectl versions
==============================================================================================

.. seealso::

   - https://github.com/kubernetes/kubectl/releases

.. toctree::
   :maxdepth: 3

   0.18.2/0.18.2
