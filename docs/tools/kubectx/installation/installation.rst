

.. _kubectx_installation:

==============================================================================================
kubectx installation
==============================================================================================

.. seealso::

   - https://github.com/ahmetb/kubectx


Example installation steps
============================

::

    sudo git clone https://github.com/ahmetb/kubectx /opt/kubectx
    sudo ln -s /opt/kubectx/kubectx /usr/local/bin/kubectx
    sudo ln -s /opt/kubectx/kubens /usr/local/bin/kubens


::

    $ sudo git clone https://github.com/ahmetb/kubectx /opt/kubectx

::

    Clonage dans '/opt/kubectx'...
    remote: Enumerating objects: 481, done.
    remote: Counting objects: 100% (481/481), done.
    remote: Compressing objects: 100% (273/273), done.
    remote: Total 977 (delta 301), reused 335 (delta 162), pack-reused 496
    Réception d'objets: 100% (977/977), 723.08 KiB | 1.64 MiB/s, fait.
    Résolution des deltas: 100% (563/563), fait.

::

    $ sudo ln -s /opt/kubectx/kubectx /usr/local/bin/kubectx
    $ sudo ln -s /opt/kubectx/kubens /usr/local/bin/kubens

::

    $ which kubectx
    /usr/local/bin/kubectx

::

    $ which kubens

::

    /usr/local/bin/kubens
