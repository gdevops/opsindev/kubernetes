

.. _kubens:

========================================================================
kubens kubens is a utility to switch between Kubernetes namespaces
========================================================================




$ kubens --help
==================


::

    kubens --help


::

    USAGE:
      kubens                    : list the namespaces in the current context
      kubens <NAME>             : change the active namespace of current context
      kubens -                  : switch to the previous namespace in this context
      kubens -c, --current      : show the current namespace
      kubens -h,--help          : show this message


Usage
======

::

    $ kubens kube-system
    Context "test" set.
    Active namespace is "kube-system".

    $ kubens -
    Context "test" set.
    Active namespace is "default".
