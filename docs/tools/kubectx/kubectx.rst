
.. index::
   ! kubectx
   ! kubens

.. _kubectx:

==================================================================================================================================================
**kubectx** (Faster way to switch between clusters and namespaces in kubectl) and **kubens** (a utility to switch between Kubernetes namespaces)
==================================================================================================================================================

.. seealso::

   - https://github.com/ahmetb/kubectx


.. toctree::
   :maxdepth: 3

   installation/installation
   commands/commands
   versions/versions
