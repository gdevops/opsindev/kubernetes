
.. index::
   ! kubectl-aliases

.. _kubectl_aliases:

===============================================================================
**kubectx-aliases** (Programmatically generated handy kubectl aliases)
===============================================================================

.. seealso::

   - https://github.com/ahmetb/kubectl-aliases
   - https://raw.githubusercontent.com/ahmetb/kubectl-alias/master/.kubectl_aliases




Installation
==============

You can directly download the .kubectl_aliases file and save it in your
$HOME directory, then edit your .bashrc/.zshrc file with:

::

    [ -f ~/.kubectl_aliases ] && source ~/.kubectl_aliases

Print the full command before running it: Add this to your .bashrc or .zshrc file:

function kubectl() { echo "+ kubectl $@">&2; command kubectl $@; }


.. _aliases_kubectl_get:

Liste des alias avec **kubectl get**
=======================================

.. literalinclude:: kubectl_aliases_get.txt
   :linenos:


.. _aliases_kubectl_v_get:

Liste des alias **ne commençant pas par kubectl get**
========================================================

.. literalinclude:: kubectl_aliases_v_get.txt
   :linenos:
