.. index::
   pair: k9s ; Tools

.. _k9s:

==============================================================================================
**k9s** Kubernetes CLI To Manage Your Clusters In Style !
==============================================================================================

.. seealso::

   - https://github.com/derailed/k9s
   - https://k9scli.io/

.. toctree::
   :maxdepth: 3


   versions/versions
