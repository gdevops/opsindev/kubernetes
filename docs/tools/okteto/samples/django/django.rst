
.. index::
   pair: okteto ; Django


.. _okteto_django_sample:

==============================================================================================
okteto Django sample (Django + Celery Sample App)
==============================================================================================

.. seealso::

   - https://github.com/okteto/samples
   - https://github.com/okteto/samples/tree/master/django
   - https://medium.com/okteto/developing-a-django-celery-in-kubernetes-354a45bde67c
