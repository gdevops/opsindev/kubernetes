
.. index::
   pair: okteto ; samples


.. _okteto_samples:

==============================================================================================
okteto samples
==============================================================================================

.. seealso::

   - https://github.com/okteto/okteto
   - https://github.com/okteto/samples


.. toctree::
   :maxdepth: 3


   django/django
