
.. index::
   pair: okteto ; Tools


.. _okteto:

==============================================================================================
okteto (Build better applications by developing and testing your code directly in Kubernetes)
==============================================================================================

.. seealso::

   - https://github.com/okteto/okteto
   - https://x.com/oktetohq
   - https://medium.com/okteto/developing-a-django-celery-in-kubernetes-354a45bde67c


.. toctree::
   :maxdepth: 3


   samples/samples
