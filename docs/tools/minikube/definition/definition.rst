
.. _minikube_def:

==============================================================================================
minikube definition
==============================================================================================

.. seealso::

   - https://minikube.sigs.k8s.io/docs/



Definition
==========

minikube quickly sets up a local Kubernetes cluster on macOS, Linux,
and Windows.

We proudly focus on helping application developers and new Kubernetes users.


Highlights
============

- Supports the latest Kubernetes release (+6 previous minor versions)
- Cross-platform (Linux, macOS, Windows)
- Deploy as a VM, a container, or on bare-metal
- Multiple container runtimes (CRI-O, containerd, docker)
- Docker API endpoint for blazing fast image pushes
- Advanced features such as LoadBalancer, filesystem mounts, and FeatureGates
- Addons for easily installed Kubernetes applications
