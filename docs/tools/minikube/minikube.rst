.. index::
   pair: minikube ; Tools
   ! minikube

.. _minikube:

==============================================================================================
**minikube** (Run Kubernetes locally)
==============================================================================================

.. seealso::

   - https://github.com/kubernetes/minikube
   - https://minikube.sigs.k8s.io/docs/


.. toctree::
   :maxdepth: 3

   definition/definition
   installation/installation
   commands/commands
   versions/versions
