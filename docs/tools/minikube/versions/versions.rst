.. index::
   pair: minikube ; Versions

.. _minikube_versions:

==============================================================================================
**minikube** versions
==============================================================================================

.. seealso::

   - https://github.com/kubernetes/minikube/releases

.. toctree::
   :maxdepth: 3

   1.9.2/1.9.2
