.. index::
   pair: minikube ; installation

.. _minikube_installation:

==============================================================================================
minikube installation
==============================================================================================

.. seealso::

   - https://minikube.sigs.k8s.io/docs/start/



For Linux users, we provide 3 easy download options
====================================================

Binary download
------------------

::

    curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
    sudo install minikube-linux-amd64 /usr/local/bin/minikube


Debian package
------------------

::

    curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_1.9.2-0_amd64.deb
    sudo dpkg -i minikube_1.9.2-0_amd64.deb


Examples
==========

.. toctree::
   :maxdepth: 3

   examples/examples
