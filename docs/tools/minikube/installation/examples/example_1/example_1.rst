
.. _minikube_example_1:

==============================================================================================
minikube installation example 1 (2020-04-25)
==============================================================================================

.. seealso::

   - https://minikube.sigs.k8s.io/docs/start/




Start your cluster
====================


::

    $ minikube start

::

    😄  minikube v1.9.2 sur Debian 10.3
    ✨  Choix automatique du driver docker
    👍  Starting control plane node m01 in cluster minikube
    🚜  Pulling base image ...
    💾  Downloading Kubernetes v1.18.0 preload ...
        > preloaded-images-k8s-v2-v1.18.0-docker-overlay2-amd64.tar.lz4: 542.91 MiB
    🔥  Creating Kubernetes in docker container with (CPUs=2) (12 available), Memory=3900MB (15817MB available) ...
    🐳  Préparation de Kubernetes v1.18.0 sur Docker 19.03.2...
        ▪ kubeadm.pod-network-cidr=10.244.0.0/16
    🌟  Installation des addons: default-storageclass, storage-provisioner
    🏄  Terminé ! kubectl est maintenant configuré pour utiliser "minikube".
    💡  Pour des résultats optimaux, installez kubectl à l'adresse suivante : https://kubernetes.io/docs/tasks/tools/install-kubectl/
