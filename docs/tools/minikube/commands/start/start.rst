.. index::
   pair: cluster ; start
   ! kubernetes cluster

.. _minikube_start:

==============================================================================================
minikube **start** (Starts a local kubernetes cluster)
==============================================================================================

.. seealso::

   - https://minikube.sigs.k8s.io/docs/commands/start/
   - https://github.com/kubernetes/minikube/blob/master/site/content/en/docs/commands/start.md




Starts a local kubernetes cluster
==================================

::

    minikube start


minikube help start
======================

::

    $ minikube help start

::

    Démarre un cluster Kubernetes local.

    Options:
          --addons=[]: Enable addons. see `minikube addons list` for a list of valid addon names.
          --apiserver-ips=[]: A set of apiserver IP Addresses which are used in the generated certificate for kubernetes.
    This can be used if you want to make the apiserver available from outside the machine
          --apiserver-name='minikubeCA': The authoritative apiserver hostname for apiserver certificates and connectivity.
    This can be used if you want to make the apiserver available from outside the machine
          --apiserver-names=[]: A set of apiserver names which are used in the generated certificate for kubernetes.  This
    can be used if you want to make the apiserver available from outside the machine
          --apiserver-port=8443: Port d'écoute du serveur d'API.
          --auto-update-drivers=true: If set, automatically updates drivers to the latest version. Defaults to true.
          --cache-images=true: If true, cache docker images for the current bootstrapper and load them into the machine.
    Always false with --driver=none.
          --container-runtime='docker': The container runtime to be used (docker, crio, containerd).
          --cpus=2: Number of CPUs allocated to Kubernetes.
          --cri-socket='': The cri socket path to be used.
          --delete-on-failure=false: If set, delete the current cluster if start fails and try again. Defaults to false.
          --disable-driver-mounts=false: Désactive les installations de systèmes de fichiers fournies par les
    hyperviseurs.
          --disk-size='20000mb': Disk size allocated to the minikube VM (format: <number>[<unit>], where unit = b, k, m or
    g).
          --dns-domain='cluster.local': Nom du domaine DNS du cluster utilisé dans le cluster Kubernetes.
          --dns-proxy=false: Active le proxy pour les requêtes DNS NAT (pilote VirtualBox uniquement).
          --docker-env=[]: Variables d'environment à transmettre au daemon Docker (format : clé = valeur).
          --docker-opt=[]: Spécifie des indicateurs arbitraires à transmettre au daemon Docker (format : clé = valeur).
          --download-only=false: Si la valeur est "true", téléchargez les fichiers et mettez-les en cache uniquement pour
    une utilisation future. Ne lancez pas d'installation et ne commencez aucun processus.
          --driver='': Driver is one of: virtualbox, parallels, vmwarefusion, kvm2, vmware, none, docker, podman
    (experimental) (defaults to auto-detect)
          --dry-run=false: dry-run mode. Validates configuration, but does not mutate system state
          --embed-certs=false: if true, will embed the certs in kubeconfig.
          --enable-default-cni=false: Enable the default CNI plugin (/etc/cni/net.d/k8s.conf). Used in conjunction with
    "--network-plugin=cni".
          --extra-config=: A set of key=value pairs that describe configuration that may be passed to different components.
            The key should be '.' separated, and the first part before the dot is the component to apply the configuration to.
            Valid components are: kubelet, kubeadm, apiserver, controller-manager, etcd, proxy, scheduler
            Valid kubeadm parameters: ignore-preflight-errors, dry-run, kubeconfig, kubeconfig-dir, node-name, cri-socket,
    experimental-upload-certs, certificate-key, rootfs, skip-phases, pod-network-cidr
          --feature-gates='': Ensemble de paires clé = valeur qui décrivent l'entrée de configuration pour des
    fonctionnalités alpha ou expérimentales.
          --force=false: Oblige minikube à réaliser des opérations possiblement dangereuses.
          --host-dns-resolver=true: Active le résolveur d'hôte pour les requêtes DNS NAT (pilote VirtualBox uniquement).
          --host-only-cidr='192.168.99.1/24': Méthode CIDR à exploiter pour la VM minikube (pilote virtualbox uniquement).
          --host-only-nic-type='virtio': NIC Type used for host only network. One of Am79C970A, Am79C973, 82540EM, 82543GC,
    82545EM, or virtio (virtualbox driver only)
          --hyperkit-vpnkit-sock='': Emplacement du socket VPNKit exploité pour la mise en réseau. Si la valeur est vide,
    désactive Hyperkit VPNKitSock. Si la valeur affiche "auto", utilise la connexion VPNKit de Docker pour Mac. Sinon,
    utilise le VSock spécifié (pilote hyperkit uniquement).
          --hyperkit-vsock-ports=[]: Liste de ports VSock invités qui devraient être exposés comme sockets sur l'hôte
    (pilote hyperkit uniquement).
          --hyperv-external-adapter='': External Adapter on which external switch will be created if no external switch is
    found. (hyperv driver only)
          --hyperv-use-external-switch=false: Whether to use external switch over Default Switch if virtual switch not
    explicitly specified. (hyperv driver only)
          --hyperv-virtual-switch='': Nom du commutateur virtuel hyperv. La valeur par défaut affiche le premier
    commutateur trouvé (pilote hyperv uniquement).
          --image-mirror-country='': Code pays du miroir d'images à utiliser. Laissez ce paramètre vide pour utiliser le
    miroir international. Pour les utilisateurs situés en Chine continentale, définissez sa valeur sur "cn".
          --image-repository='': Alternative image repository to pull docker images from. This can be used when you have
    limited access to gcr.io. Set it to "auto" to let minikube decide one for you. For Chinese mainland users, you may use
    local gcr.io mirrors such as registry.cn-hangzhou.aliyuncs.com/google_containers
          --insecure-registry=[]: Insecure Docker registries to pass to the Docker daemon.  The default service CIDR range
    will automatically be added.
          --install-addons=true: If set, install addons. Defaults to true.
          --interactive=true: Allow user prompts for more information

    --iso-url=[https://storage.googleapis.com/minikube/iso/minikube-v1.9.0.iso,https://github.com/kubernetes/minikube/releases/download/v1.9.0/minikube-v1.9.0.iso,https://kubernetes.oss-cn-hangzhou.aliyuncs.com/minikube/iso/minikube-v1.9.0.iso]:
    Locations to fetch the minikube ISO from.
          --keep-context=false: Cela permet de conserver le contexte kubectl existent et de créer un contexte minikube.
          --kubernetes-version='': The kubernetes version that the minikube VM will use (ex: v1.2.3, 'stable' for v1.18.0,
    'latest' for v1.18.0). Defaults to 'stable'.
          --kvm-gpu=false: Active l'assistance expérimentale du GPU NVIDIA dans minikube.
          --kvm-hidden=false: Masque la signature de l'hyperviseur de l'invité dans minikube (pilote kvm2 uniquement).
          --kvm-network='default': Nom du réseau de la KVM (pilote kvm2 uniquement).
          --kvm-qemu-uri='qemu:///system': URI de connexion QEMU de la KVM (pilote kvm2 uniquement).
          --memory='': Amount of RAM to allocate to Kubernetes (format: <number>[<unit>], where unit = b, k, m or g).
          --mount=false: This will start the mount daemon and automatically mount files into minikube.
          --mount-string='/home/pvergain:/minikube-host': The argument to pass the minikube mount command on start.
          --nat-nic-type='virtio': NIC Type used for host only network. One of Am79C970A, Am79C973, 82540EM, 82543GC,
    82545EM, or virtio (virtualbox driver only)
          --native-ssh=true: Use native Golang SSH client (default true). Set to 'false' to use the command line 'ssh'
    command when accessing the docker machine. Useful for the machine drivers when they will not start with 'Waiting for
    SSH'.
          --network-plugin='': The name of the network plugin.
          --nfs-share=[]: Dossiers locaux à partager avec l'invité par des installations NFS (pilote hyperkit uniquement).
          --nfs-shares-root='/nfsshares': Emplacement permettant d'accéder aux partages NFS en mode root, la valeur par
    défaut affichant /nfsshares (pilote hyperkit uniquement).
          --no-vtx-check=false: Désactive la vérification de la disponibilité de la virtualisation du matériel avant le
    démarrage de la VM (pilote virtualbox uniquement).
      -n, --nodes=1: The number of nodes to spin up. Defaults to 1.
          --preload=true: If set, download tarball of preloaded images if available to improve start time. Defaults to true.
          --registry-mirror=[]: Miroirs de dépôt à transmettre au daemon Docker.
          --service-cluster-ip-range='10.96.0.0/12': Méthode CIDR à exploiter pour les adresses IP des clusters du
    service.
          --uuid='': Fournit l'identifiant unique universel (UUID) de la VM pour restaurer l'adresse MAC (pilote hyperkit
    uniquement).
          --vm=false: Filter to use only VM Drivers
          --vm-driver='': DEPRECATED, use `driver` instead.
          --wait=[apiserver,system_pods]: comma separated list of kubernetes components to verify and wait for after
    starting a cluster. defaults to "apiserver,system_pods", available options: "apiserver,system_pods,default_sa" . other
    acceptable values are 'all' or 'none', 'true' and 'false'
          --wait-timeout=6m0s: max time to wait per Kubernetes core services to be healthy.

    Usage:
      minikube start [flags] [options]

    Use "minikube options" for a list of global command-line options (applies to all commands).
