.. index::
   pair: minikube ; commands

.. _minikube_commands:

==============================================================================================
minikube commands
==============================================================================================

.. seealso::

   - https://minikube.sigs.k8s.io/docs/commands/
   - https://github.com/kubernetes/minikube/tree/master/site/content/en/docs/commands


.. toctree::
   :maxdepth: 3

   dashboard/dashboard
   help/help
   kubectl/kubectl
   options/options
   start/start
   status/status
