.. index::
   pair: options ; minikube

.. _minikube_options:

=======================================================================================================
minikube **options** (Show a list of global command-line options)
=======================================================================================================

.. seealso::

   - https://minikube.sigs.k8s.io/docs/commands/options/
   - https://github.com/kubernetes/minikube/edit/master/site/content/en/docs/commands/options.md




Access the kubernetes options running within the minikube cluster
================================================================================

::

    minikube options

::

    The following options can be passed to any command:

          --alsologtostderr=false: log to standard error as well as files
      -b, --bootstrapper='kubeadm': The name of the cluster bootstrapper
            that will set up the kubernetes cluster.
          --log_backtrace_at=:0: when logging hits line file:N,
            emit a stack trace
          --log_dir='': If non-empty, write log files in this directory
          --logtostderr=false: log to standard error instead of files
      -p, --profile='minikube': The name of the minikube VM being used.
            This can be set to allow having multiple instances of minikube
            independently.
          --stderrthreshold=2: logs at or above this threshold go to stderr
      -v, --v=0: log level for V logs
          --vmodule=: comma-separated list of pattern=N settings for
            file-filtered logging
