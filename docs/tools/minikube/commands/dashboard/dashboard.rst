.. index::
   pair: dashboard ; minikube

.. _minikube_dashboard:

=======================================================================================================
minikube **dashboard** (Access the kubernetes dashboard running within the minikube cluster)
=======================================================================================================

.. seealso::

   - https://minikube.sigs.k8s.io/docs/commands/dashboard/
   - https://github.com/kubernetes/minikube/edit/master/site/content/en/docs/commands/dashboard.md




Access the kubernetes dashboard running within the minikube cluster
================================================================================

::

    minikube dashboard


minikube help dashboard
=========================

::

    $ minikube help dashboard

::

    Access the kubernetes dashboard running within the minikube cluster

    Options:
          --url=false: Display dashboard URL instead of opening a browser

    Usage:
      minikube dashboard [flags] [options]

    Use "minikube options" for a list of global command-line options (applies to all commands).

Examples
==========

.. toctree::
   :maxdepth: 3

   examples/examples
