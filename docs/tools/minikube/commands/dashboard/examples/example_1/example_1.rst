
.. _minikube_dashboard_example_1:

==============================================================================================
minikube dashboard (2020-04-25)
==============================================================================================

.. seealso::

   - http://127.0.0.1:35979/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/

::

    $ minikube dashboard

::

    🔌  Enabling dashboard ...
    🤔  Verifying dashboard health ...
    🚀  Launching proxy ...
    🤔  Verifying proxy health ...
    🎉  Opening http://127.0.0.1:35979/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/ in your default browser...


.. figure:: web_dashboard.png
   :align: center

   http://127.0.0.1:35979/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/
