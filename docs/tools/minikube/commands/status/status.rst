.. index::
   pair: minikube ; status

.. _minikube_status:

==============================================================================================
minikube **status** (Gets the status of a local kubernetes cluster.)
==============================================================================================

.. seealso::

   - https://minikube.sigs.k8s.io/docs/commands/status/
   - https://github.com/kubernetes/minikube/blob/master/site/content/en/docs/commands/status.md




Gets the status of a local kubernetes cluster
===============================================

::

    minikube status


minikube help start
======================

::

    $ minikube help status

::

    Gets the status of a local kubernetes cluster.
        Exit status contains the status of minikube's VM, cluster
        and kubernetes encoded on it's bits in this order
        from right to left.

        Eg: 7 meaning: 1 (for minikube NOK) + 2 (for cluster NOK) + 4 (for kubernetes NOK)

    Options:
      -f, --format='{{.Name}}
    host: {{.Host}}
    kubelet: {{.Kubelet}}
    apiserver: {{.APIServer}}
    kubeconfig: {{.Kubeconfig}}

    ': Go template format string for the status output.  The format for Go templates can be found here:
    https://golang.org/pkg/text/template/
    For the list accessible variables for the template, see the struct values here:
    https://godoc.org/k8s.io/minikube/cmd/minikube/cmd#Status
      -o, --output='text': minikube status --output OUTPUT. json, text

    Usage:
      minikube status [flags] [options]

    Use "minikube options" for a list of global command-line options (applies to all commands).
