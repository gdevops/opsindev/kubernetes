.. index::
   pair: rancher2 ; Tools

.. _rancher2:

==============================================================================================
rancher2 (Complete container management platform)
==============================================================================================

.. seealso::

   - https://rancher.com/docs/rancher/v2.x/en/
   - https://github.com/rancher/rancher
   - http://2019.devops-dday.com/Workshop.html


.. figure:: rancher-logo.png
   :align: center
