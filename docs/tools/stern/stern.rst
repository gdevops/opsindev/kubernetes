.. index::
   ! stern

.. _stern:

==============================================================================================
**stern** ⎈ Multi pod and container log tailing for Kubernetes
==============================================================================================

.. seealso::

   - https://github.com/wercker/stern


.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
