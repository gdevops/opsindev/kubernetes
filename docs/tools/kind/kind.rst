.. index::
   pair: KinD ; Tools
   ! KinD

.. _kind:

==============================================================================================
**KinD** (**Kubernetes in Docker**, local clusters for testing Kubernetes)
==============================================================================================

.. seealso::

   - https://github.com/kubernetes-sigs/kind
   - https://kind.sigs.k8s.io/
   - https://github.com/kubernetes-sigs/kind#installation-and-usage
   - https://www.youtube.com/watch?v=RTkoQiJ1x8w
   - https://x.com/CloudNativeFdn
   - https://landscape.cncf.io/category=certified-kubernetes-installer&format=card-mode&grouping=category
   - https://landscape.cncf.io/category=certified-kubernetes-installer&format=card-mode&grouping=category&selected=kind

.. figure:: kind_logo.png
   :align: center
   :width: 300

.. figure:: cncf_kind.png
   :align: center

   https://landscape.cncf.io/category=certified-kubernetes-installer&format=card-mode&grouping=category&selected=kind


.. toctree::
   :maxdepth: 3

   description/description
   versions/versions
