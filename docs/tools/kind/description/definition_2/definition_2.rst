
.. _kind_def2:

============================================
KinD definition by KinD
============================================





Definition
===========

kind is a tool for running local Kubernetes clusters using Docker
container *nodes*

kind was primarily designed for testing Kubernetes itself, but may be
used for local development or CI.
