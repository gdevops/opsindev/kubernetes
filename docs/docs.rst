
.. index::
   pair: Kubernetes ; docs


.. _kubernetes_docs:

=====================
Docs
=====================

.. seealso::


   - https://kubernetes.io/docs/home/

.. toctree::
   :maxdepth: 3

   why/why
   api/api
   awsome/awsome
   tutoriels/tutoriels
   helm/helm
   operators/operators
   tools/tools
   people/people
   books/books
   used_by/used_by
