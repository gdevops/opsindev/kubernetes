.. index::
   pair: Kubernetes ; operators

.. _kubernetes_operators:

=====================
Kubernetes operators
=====================

.. seealso::

   - https://github.com/operator-framework/awesome-operators
   - https://github.com/operator-framework
   - https://dzone.com/articles/kubernetes-operators-what-are-they
