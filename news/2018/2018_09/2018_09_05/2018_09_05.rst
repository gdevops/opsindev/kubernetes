


.. index::
   pair: HashiCorp Consul ; Kubernetes


.. _hashi_corp_2018:

===========================================================================================
Announcing HashiCorp Consul + Kubernetes
===========================================================================================


.. seealso::

   - https://www.hashicorp.com/blog/consul-plus-kubernetes/




Description
=============

Sep 05 2018 Mitchell Hashimoto

We're excited to announce multiple features that deeply integrate HashiCorp
Consul with Kubernetes.
This post will share the initial set of features that will be released
in the coming weeks.

The features include an official Helm Chart for installing Consul on
Kubernetes, automatic syncing of Kubernetes services with Consul
(and vice versa), auto-join for external Consul agents to join a cluster
in Kubernetes, injectors so pods are automatically secured with Connect,
and support for Envoy.

In addition to natively integrating with Kubernetes, these features
help solve important cross-cluster challenges between multiple
Kubernetes clusters as well as non-Kubernetes services interacting
with Kubernetes services.
We're excited to share this work with you.


Integrating with Kubernetes
==============================

We're currently integrating closely with Kubernetes across multiple products.

We see opportunities to solve challenges for pure Kubernetes users by
making our products easier to run as well as integrating with and
enhancing Kubernetes features.

A core tenet of this integration is to enhance existing features rather
than replace.

Features such as Services, ConfigMaps, Secrets, and more are part of
the core Kubernetes workflow. Higher level tools and extensions leverage
these core primitives.
Therefore, we're also integrating with and enhancing these core primitives.

For example, the Consul catalog sync converts external services in
Consul's catalog into first-class Kubernetes Service resources.
Applications running in Kubernetes can then discover and connect
to non-Kubernetes services natively.
