


.. index::
   pair: Kubernetes ; 2018-09


.. _kubernetes_2018_09:

=========================
Kubernetes news 2018-09
=========================

.. seealso::

   - https://mjbright.github.io/Talks/
   - :ref:`mickael_bright`


.. toctree::
   :maxdepth: 3

   2018_09_04/2018_09_04
   2018_09_05/2018_09_05
   2018_09_13/2018_09_13
   2018_09_20/2018_09_20
   2018_09_26/2018_09_26
