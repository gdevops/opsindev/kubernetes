

.. _delamarche_2022_09_01:

================================================================================================
2022-09-01 **Kubernetes : comment éviter une catastrophe involontaire ?** par Delamarche Jérôme
================================================================================================

- https://connect.ed-diamond.com/gnu-linux-magazine/glmf-259/kubernetes-comment-eviter-une-catastrophe-involontaire


Auteur Delamarche Jérôme
=============================

- https://connect.ed-diamond.com/auteur/delamarche-jerome
