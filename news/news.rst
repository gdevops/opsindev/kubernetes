


.. index::
   pair: Kubernetes ; News


.. _kubernetes_news:

=====================
News
=====================

.. toctree::
   :maxdepth: 5

   2022/2022
   2020/2020
   2019/2019
   2018/2018
