
.. _depl_kubernetes_2020_04_07:

========================================================================================
Low overhead, on-demand Kubernetes clusters deployed on CI Workers Nodes with **KinD**
========================================================================================

.. seealso::

   - https://www.praqma.com/stories/testing-kubernetes-deployments-within-ci-pipelines/




On-Demand Private Kubernetes Cluster with KinD
=================================================

Kubernetes-in-Docker (KinD) is an implementation of a Kubernetes cluster
using Docker-in-Docker (DIND) technology. Docker-in-docker means that
we can run containers inside containers and those inner containers
are only visible inside the outer container.

KinD uses this to implement a cluster by using the outer container to
implement Kubernetes cluster nodes.

When a Kubernetes POD is started on a node it is implemented with
containers inside the outer node container.

...

However, there are still many cases where testing with a KinD Kubernetes
cluster is ideal, e.g. when you have Kubernetes-related artifacts to test
like a Helm chart or YAML manifests, and when an external CI/staging
Kubernetes cluster involves too much maintenance overhead or is too
resource in-efficient.
