
.. index::
   pair: Kubernetes ; Martin Heinz

.. _kubernetes_python_2020_04_15:

==================================================================================================
**Deploy Any Python Project to Kubernetes** by Martin Heinz (https://x.com/Martin_Heinz\_)
==================================================================================================

.. seealso::

   - https://martinheinz.dev/blog/20
   - https://martinheinz.dev/
   - https://github.com/MartinHeinz/python-project-blueprint
   - https://martinheinz.dev/blog/17
   - https://x.com/Martin_Heinz\_



Introduction
===============

As your project grows, it might get to the point that it becomes too
hard to handle with just single VM or some simple SaaS solution.

You can solve that by switching to more robust solution like Kubernetes.

That might however, be little too complex if you are not familiar with
it's concepts or if just never used it before.

So, to help you out - in this article - we will go over all you need to
get yourself started and have your Python project deployed on cluster,
including cluster setup, all the Kubernetes manifests and some extra
automation to make your life easier down the roadbv!

This is a follow up to previous article(s) about Automating Every Aspect
of Your Python Project, so you might want check that_ out before reading
this one.

TL;DR: Here is my repository with full source code and docs: https://github.com/MartinHeinz/python-project-blueprint

.. _that:  https://martinheinz.dev/blog/17



Comfy Development Setup
==========================

To be productive in your development process, you need to have comfy
local development setup.
Which in this case means having simple to use Kubernetes on local,
closely mirroring your real, production cluster and for that,
we will use KinD:

:ref:`KinD (Kubernetes-in-Docker) <kind>` , as the name implies, runs
Kubernetes clusters in Docker containers.

It is the official tool used by Kubernetes maintainers for Kubernetes
v1.11+ conformance testing.
It supports multi-node clusters as well as HA clusters. Because it runs
K8s in Docker, KinD can run on Windows, Mac, and Linux. So, you can run
it anywhere, you just need Docker installed.

So, let's install KinD::


    curl -Lo ./kind https://github.com/kubernetes-sigs/kind/releases/download/v0.7.0/kind-$(uname)-amd64

::

      % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                     Dload  Upload   Total   Spent    Left  Speed
    100   629  100   629    0     0   5376      0 --:--:-- --:--:-- --:--:--  5376
    100 9136k  100 9136k    0     0  1552k      0  0:00:05  0:00:05 --:--:-- 1793k


    ::

    ~ $ chmod +x ./kind
    ~ $ sudo mv ./kind /usr/local/bin/kind

::

    ~ $ kind --version

::

    kind version 0.7.0


With that, we are ready to setup our cluster.
For that we will need following YAML file:

.. code-block:: yaml
   :linenos:

    kind: Cluster
    apiVersion: kind.x-k8s.io/v1alpha4
    nodes:
    - role: control-plane
      kubeadmConfigPatches:
      - |
        kind: InitConfiguration
        nodeRegistration:
          kubeletExtraArgs:
            node-labels: "ingress-ready=true"
            authorization-mode: "AlwaysAllow"
      extraPortMappings:
      - containerPort: 80
        hostPort: 80
        protocol: TCP
      - containerPort: 443
        hostPort: 443
        protocol: TCP
    - role: worker
    - role: worker


This manifest describes our cluster.

It will have 3 nodes - control plane (role: control-plane) and 2 workers
role: worker.

We are also giving it a few more settings and arguments to make it
possible to setup ingress controller later, so that we can have HTTPS
on this cluster.

All you need to know about those settings, is that extraPortMappings
tells cluster to forward ports from the host to an ingress controller
running on a node.

Note: All the manifests for both cluster and Python application are
available in `my repo`_ here in **k8s directory**.

.. _`my repo`:  https://github.com/MartinHeinz/python-project-blueprint
