
.. index::
   pair: Jérôme Petazzoni ; 2020-04-15
   pair: Nixery; Kubernetes

.. _kub_jpetazzo_2020_04_15:

=================================================================================================
**Creating Optimized Images for Docker and Kubernetes** with **nixery** by Jérôme Petazzoni
=================================================================================================

.. seealso::

   - :ref:`kub_jpetazzo_2020_04_15`



To Nix or not to Nix ?
=========================

Ce blog post ayant vocation à rester court (on me dit dans l’oreillette
que c’est raté😅), je n’ai pas la place d’expliquer en détail comment
utiliser Nix pour générer l’image parfaite.

Mais j’espère que cette petite démo de Nix vous permet de vous faire
une idée.

À vous d’approfondir le sujet si ça vous a ouvert l’appétit !

Pour ma part, je compte bien utiliser Nixery quand j’ai besoin d’images
de conteneurs pour un usage précis, en particulier avec Kubernetes.

Prenons un exemple, si j’ai besoin d’une image avec curl, tar et la CLI de AWS.

Mon approche traditionnelle aurait été d’utiliser alpine puis d’exécuter
pip install awscli.
Mais avec Nixery, je vais simplement pouvoir utiliser l’image
nixery.dev/shell/curl/gnutar/awscli !

::

    docker run -ti nixery.dev/shell/curl/gnutar/awscli sh
