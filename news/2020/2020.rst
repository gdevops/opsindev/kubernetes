.. index::
   pair: Kubernetes ; 2020

.. _kubernetes_2020:

=====================
2020
=====================

.. toctree::
   :maxdepth: 3

   11/11
   10/10
   04/04
