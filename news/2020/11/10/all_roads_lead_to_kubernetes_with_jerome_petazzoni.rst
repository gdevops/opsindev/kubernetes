
.. index::
   pair: Kubernetes ; 2020-11-10

.. _petazzoni_2020_11_10:

=====================================================
All Roads Lead to Kubernetes with Jérôme Petazzoni
=====================================================

.. seealso::

   - https://semaphoreci.com/blog/all-roads-lead-to-kubernetes-with-jerome-petazzoni



The journey: from SRE manager to evangelist
==============================================

:Darko (00:02): Hello, and welcome to Semaphore, a podcast for developers
    about building great products.
    Today, I’m excited to welcome Jérôme Petazzoni.
    Hi, Jérôme. It’s great to have you.



Hi, I am Jérôme Petazzoni, I worked for a decade in France in
various companies around infrastructure and open-source, and then
I moved to California to join this little startup that you might
know under the name of Docker. But when I joined it, it was not
Docker yet, it was dotCloud.

I worked there for seven years, moving from infrastructure to
managing a small team of a SREs, to then being Docker’s first evangelist,
then delivering training and workshop on Docker and then orchestration
and then Kubernetes.
Eventually, at the beginning of 2018, exactly seven years after joining,
I quit because I was dealing with a pretty bad bout of depression and
burnout.

So, I needed some time to recover from that. And after that, I decided
to do training mostly on Kubernetes but sometimes on Docker as well.


When Kubernetes became the next big thing
==============================================

:Darko(07:11): When did you realize that Kubernetes could be a big thing?
    Can you talk a bit about that in the journey of Docker, when Kubernetes
    came about?


Jérôme: I think as soon as in 2014, we had folks from the original team
of Kubernetes coming to the office and having conversations with the
Docker team. Unfortunately, there were two separate lines here, where
Docker thought, well, Kubernetes just seems way too complicated for
developers.

We know developer experience, we have successfully shipped Docker itself
and then Docker desktop. So we got this, we’re going to make an amazing
orchestration product that’s going to be way easier to use.

It’s true that Swarm was and still is easier to use than Kubernetes, but
it’s also true that it’s more limited in some ways.

At the end of my time at Docker at the end of 2017 I had successfully
delivered a Kubernetes workshop at Linux Open Source Summit.
I was asked, “Hey, can you go and deliver that workshop to our engineering
teams at Docker?”
And that gave me the opportunity to go to really where Docker had this
office with a bunched around the whole enterprise support team.

And there was a moment when one of them said something like, “Wow, this
is going to be amazing to operate.” and they were serious about it.
It’s a little bit like, instead of having this black box that we had with
Swarm, where it works, but if you need to tinker and do something that
goes a little bit outside of the spec, it’s just impossible. With
Kubernetes, you can see how things are working. And once you take the
time to understand it, it becomes extremely powerful.


For me, that was the moment when I was like, “Okay, if these folks who
are working with our customers every day, troubleshooting their issues,
helping them solve all kinds of challenges if these folks think that
Kubernetes is going to be great to operate, then this is it. It’s a thing.

I’m convinced.” Right, it might be complicated. Sure. But guess what?
Linux is also complicated, for instance

Building Kubernetes knowledge
===============================

:Darko (11:11): If you take it three years later, what would you say
    about maybe paths to understanding and learning Kubernetes?
    Has it got easier, more approachable for people?

Jérôme: I would say that as a whole, if we take the whole cloud-native
landscape, it got more complicated, because back then, well, 2017, it
was already more than Kubernetes.

But we kept adding things, like Kubernetes, and then perhaps you need
a service mesh or some serverless framework to execute functions.

If you want, you can keep adding layers and layers and layers.

So, if you want to understand the whole thing, yes, it got more complicated.

But, if we stick to Kubernetes itself, I feel like it didn’t get more
complicated. It got a little bit simpler.


:Darko: What’s easier for people to grasp, what they spend more time on,
    what are they more excited about?

Jérôme: I think folks will get excited by different topics.

For instance, when I talk about Docker, at some point in my Docker
curriculum, I teach how to optimize the size of images.

But, I also try to explain, your goal isn’t always to get the smallest
possible image.
I’m just going to set that as a goal and show you how we do that.

But sometimes, you will want to optimize for faster builds, or sometimes
you will want to constrain yourself to a specific set of tools so that
you can build, not only on Intel machines but also ARM or whatever.

So, I just set that goal. And then, we go through, let’s do multi-stage,
and let’s try to do static binaries, and let’s try to use Alpine and
musl instead of WNC, all these little tweaks changes.


One day I was wondering, “Is this necessary? Should I really bother
people with that kind of detail?” Because we are super far from containers
at this point.
Do people need to know about dynamic linking and these little things?
I was almost about to decide to remove that from my training.
I saw a tweet from someone saying that they had been trying to get a
Go Binary into a container image and having exactly that kind of
problem with linking. It took them like 30 minutes to find what was wrong.

And I was like, okay. If that person is having this difficulty, I
should keep including that in my content because it is great and useful.

And getting that constant feedback for me is pretty important, realizing
what kind of problems people face.

How to achieve a 5-second build time
=======================================

In the same vein is trying to get the fastest build time on the Kubernetes
cluster. By build time, I mean, you change a line of code on your machine,
and how long does it take between the moment when you change your line
of code and the moment when the code is running on the Kubernetes cluster?

And you start with, I don’t know, maybe five or 10 minutes because you’re
doing builds and you’re pushing images, et cetera.
And then, you try to shave off time from that. Again, I think they have
something like three or four seconds, which seems completely ridiculous.
If you want to read more about that in any search engine look for,
The Quest for the Fastest Deployment Time, by Ellen Körbes. It’s super interesting

You kind of iterate, and along the way you learn about dozens of things
that you thought would be, “I will never need that in my engineering
career,” and then you’re like, “Well, after all, let me come back to that
because…”

Self-compressing binaries and things like that. And you’re like, “Yep,
there are scenarios where it is actually pretty useful.”

We want to iterate really fast. I want to make a change. I want to see
that change immediately. If it takes five seconds, it means I can try
hundreds of changes during my day and see the impact of that change.

If it takes 20 minutes before I can see my change, then I’m going to be
able to only do a few of them during the day. And so, I will not progress
as fast, basically


Kubernetes is going to help smaller teams to do that instead
=================================================================

It’s like in a perfectly ideal world, we wouldn’t need to know anything
about Kubernetes; it would be an implementation detail.

But in the real world, we have to be pragmatic.

And sometimes, we will need to go and look in the Kubernetes cluster
to see what’s going on. So, I think that’s where we are.
And I think we’ll continue to see more tools, more platforms.

We have things like Heroku, Clever Cloud, if I want to talk about the
SAS versions.

I think it’s becoming increasingly important in a team to have someone
who will know how to set up all that automation, who will know how to
pick the right CI/CD platform, and how to configure everything because
I can’t just, buy one pre-made workflow.

I need to have a workflow that works for my team. And I don’t know if
we already have, or if we should have a title for that.

Is it like an automation engineer, CI/CD engineer? I don’t know.
But, I think this is becoming increasingly important, and these are
going to be extremely valuable because we’re going to talk about
someone who understands the underlying platform infrastructure.

So, who understands cloud APIs, Kubernetes, container images, maybe
Dockerfiles, or whatever else. But that also understands what the
business needs.
Do I want to have just product staging, or do I need to have multiple
preview environments?
I think this is going to be a really important role for organizations
and teams who want to ship efficiently code in this decade.
