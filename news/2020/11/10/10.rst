.. index::
   pair: Kubernetes ; 2020-11-10

.. _kubernetes_2020_11_10:

=====================
2020-11-10
=====================

.. toctree::
   :maxdepth: 3

   all_roads_lead_to_kubernetes_with_jerome_petazzoni
