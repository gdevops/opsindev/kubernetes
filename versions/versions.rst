.. index::
   pair: Kubernetes ; Versions

.. _kubernetes_versions:

=====================
Versions
=====================

.. seealso::

   - https://groups.google.com/forum/#!forum/kubernetes-announce
   - https://github.com/kubernetes/kubernetes/releases

.. toctree::
   :maxdepth: 3

   1.18.2/1.18.2
   1.18.0/1.18.0
   1.16.0/1.16.0
   1.14.2/1.14.2
   1.10.0/1.10.0
